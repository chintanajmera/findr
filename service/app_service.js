var ser = angular.module("Service");
ser.service("app_service",["$http","$q",function($http,$q){
    this.get_data = function()
    {
        var defer = $q.defer();
        $http({
           method:"GET",
           url:"data.json"
        }).success(function(response){
            defer.resolve(response);
        }).error(function(error){
            defer.reject(error);
        })
        return defer.promise;
    }
}]);
