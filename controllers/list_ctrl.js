var list = angular.module("List.Controller");

list.controller("list_ctrl",["$scope","app_service", "$sessionStorage",function($scope,app_service, $sessionStorage){
     var vm = this;
    vm.data_list = [];
    
    vm.like = like;
    
    var getdata = function()
    {
        if(!$sessionStorage.data_list)
        {
            app_service.get_data()
            .then(function(response)
            {
                vm.data_list = response;
            });
        }
        else
        {
            vm.data_list = $sessionStorage.data_list;
        }
    }();
    
    $scope.$on('newDataCollected', function(event, data) 
    {
        vm.data_list.push(data);
        $sessionStorage.data_list = vm.data_list;
    });
    
    function like(rcd)
    {
        console.log(rcd);
        if(rcd.count <= 0)
        {
            rcd.count = 1;
        }
        else if(rcd.count == 1)
        {
            rcd.count = 0;
        }
    }
        
}]);
