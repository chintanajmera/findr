var frm = angular.module("Form.Controller");
frm.controller("form_ctrl",["$scope","app_service",function($scope,app_service){
    var vm = this;
    vm.save_data = save_data;
    vm.formdata = {
        title : "",
        url: "",
        tag:""
    };
    
    function save_data()
    {
        if(vm.formdata.title && vm.formdata.tag && vm.formdata.url)
        {
            vm.new_data = {
                title : vm.formdata.title,
                url: vm.formdata.url,
                tag:vm.formdata.tag,
                count:-1
            };
            
            $scope.$emit('newDataAdded', vm.new_data);
            
            vm.formdata = {
                title : "",
                url: "",
                tag:""
            };
        }
        else
        {
            alert("All fields are mandatory");
        }
    }    
}])
