module.exports = function (grunt) 
{
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        imagemin: {
            jpg: {
                options: {
                    progressive: true
                },
                files: [{
                        expand: true,
                        cwd: 'img/',
                        src: ['**/*.jpg'],
                        dest: 'deployed/img/'
                    }]
            },
            png: {
                options: {
                    progressive: true
                },
                files: [{
                        expand: true,
                        cwd: 'img/',
                        src: ['**/*.png'],
                        dest: 'deployed/img/'
                    }]
            },
            gif: {
                options: {
                    progressive: true
                },
                files: [{
                        expand: true,
                        cwd: 'img/',
                        src: ['**/*.gif'],
                        dest: 'deployed/img/'
                    }]
            }


        },
        
        concat: {
            dist: {
                src: [
                    'js/app_config.js', // All JS in the libs folder
                    'controllers/*.js',
                    'service/*.js',// This specific file
                    'filter/*.js'// This specific file
                ],
                dest: 'deployed/js/production.js',
            }
        },
        uglify: {
            build: {
                src: 'deployed/js/production.js',
                dest: 'deployed/js/production.min.js'
            }
        },
        less: {
            options: {
                plugins: [new (require('less-plugin-autoprefix'))({browsers: ["last 2 versions"]})]
            },
            main: {
                files: {
                    'deployed/css/main.css': 'css/main.less'
                }
            }
        },
        concat_css: {
            options: {
            },
            all: {
                src: ['css/bootstrap.min.css', 'deployed/css/main.css'],
                dest: "deployed/css/production.css"
            },
        },
        cssmin: {
            my_target: {
                files: [{
                        expand: true,
                        cwd: 'deployed/css/',
                        src: ['production.css'],
                        dest: 'deployed/css',
                        ext: '.min.css'
                    }]
            }
        },
        htmlmin: {// Task
            dist: {// Target
                options: {// Target options
                    removeComments: true,
                    collapseWhitespace: true
                },
                files: [{
                        expand: true,
                        cwd: 'partials/',
                        src: ['*.html'],
                        dest: 'deployed/partials',
                        ext: '.html'
                    }]
            }
        },
        watch: {
            options: {
            },
            scripts: {
                files: ['js/**/*.js'],
                tasks: ['concat', 'uglify'],
                options: {
                    livereload: true,
                },
            },
            css: {
                files: ['css/*.less'],
                tasks: ['less', 'concat_css', 'cssmin'],
                options: {
                    livereload: true,
                }
            },
            html: {
                files: ['partials/*.html','index.html'],
                tasks: ['htmlmin'],
                options: {
                    livereload: true,
                }
            }
        },
    });
    grunt.loadNpmTasks('grunt-contrib-imagemin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('less-plugin-autoprefix');
    grunt.loadNpmTasks('less-plugin-clean-css');
    grunt.loadNpmTasks('grunt-contrib-htmlmin');
    grunt.loadNpmTasks('grunt-concat-css');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', ['concat', 'uglify', 'less', 'concat_css', 'cssmin', 'htmlmin','imagemin']);
};
