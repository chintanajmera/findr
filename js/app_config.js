var myApp = angular.module("findr",[
"ngRoute",
"Form.Controller",
"List.Controller",
"Service",
"Filter",
"ngStorage"
]);

myApp.config(["$httpProvider", "$routeProvider", function ($httpProvider, $routeProvider)
{
    $routeProvider
        .when("/", {
            templateUrl: "partials/main.html",
            controller: ""
        })
        .otherwise({
            redirectTo: "/"
        });
}]);

myApp.run(function($rootScope)
{
    $rootScope.$on('newDataAdded', function(event, data) 
    {
        $rootScope.$broadcast('newDataCollected', data);
    });
})


angular.module("Form.Controller",[]);
angular.module("List.Controller",[]);
angular.module("Service",[]);
angular.module("Filter",[]);