var myApp = angular.module("findr",[
"ngRoute",
"Form.Controller",
"List.Controller",
"Service",
"Filter",
"ngStorage"
]);

myApp.config(["$httpProvider", "$routeProvider", function ($httpProvider, $routeProvider)
{
    $routeProvider
        .when("/", {
            templateUrl: "partials/main.html",
            controller: ""
        })
        .otherwise({
            redirectTo: "/"
        });
}]);

myApp.run(function($rootScope)
{
    $rootScope.$on('newDataAdded', function(event, data) 
    {
        $rootScope.$broadcast('newDataCollected', data);
    });
})


angular.module("Form.Controller",[]);
angular.module("List.Controller",[]);
angular.module("Service",[]);
angular.module("Filter",[]);

var frm = angular.module("Form.Controller");
frm.controller("form_ctrl",["$scope","app_service",function($scope,app_service){
    var vm = this;
    vm.save_data = save_data;
    vm.formdata = {
        title : "",
        url: "",
        tag:""
    };
    
    function save_data()
    {
        if(vm.formdata.title && vm.formdata.tag && vm.formdata.url)
        {
            vm.new_data = {
                title : vm.formdata.title,
                url: vm.formdata.url,
                tag:vm.formdata.tag,
                count:-1
            };
            
            $scope.$emit('newDataAdded', vm.new_data);
            
            vm.formdata = {
                title : "",
                url: "",
                tag:""
            };
        }
        else
        {
            alert("All fields are mandatory");
        }
    }    
}])

var list = angular.module("List.Controller");

list.controller("list_ctrl",["$scope","app_service", "$sessionStorage",function($scope,app_service, $sessionStorage){
     var vm = this;
    vm.data_list = [];
    
    vm.like = like;
    
    var getdata = function()
    {
        if(!$sessionStorage.data_list)
        {
            app_service.get_data()
            .then(function(response)
            {
                vm.data_list = response;
            });
        }
        else
        {
            vm.data_list = $sessionStorage.data_list;
        }
    }();
    
    $scope.$on('newDataCollected', function(event, data) 
    {
        vm.data_list.push(data);
        $sessionStorage.data_list = vm.data_list;
    });
    
    function like(rcd)
    {
        console.log(rcd);
        if(rcd.count <= 0)
        {
            rcd.count = 1;
        }
        else if(rcd.count == 1)
        {
            rcd.count = 0;
        }
    }
        
}]);

var ser = angular.module("Service");
ser.service("app_service",["$http","$q",function($http,$q){
    this.get_data = function()
    {
        var defer = $q.defer();
        $http({
           method:"GET",
           url:"data.json"
        }).success(function(response){
            defer.resolve(response);
        }).error(function(error){
            defer.reject(error);
        })
        return defer.promise;
    }
}]);

var fltr = angular.module("Filter");
fltr.filter("app_filter",function(){
    return function(data, search_text)
    {
        var result = [];
        if(search_text)
        {
            angular.forEach(data, function(value,key)
            {
                var ttl = value.title.toLowerCase();
                var tag = value.tag.toLowerCase();
                var url = value.url.toLowerCase();
                
                if(ttl.indexOf(search_text.toLowerCase()) > -1
                        || url.indexOf(search_text.toLowerCase()) !== -1
                        || tag.indexOf(search_text.toLowerCase()) !== -1)
                {
                    result.push(value);
                }
            });
            return result;
        }
        else
        {
            return data;
        }
    }
});
