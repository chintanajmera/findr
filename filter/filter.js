var fltr = angular.module("Filter");
fltr.filter("app_filter",function(){
    return function(data, search_text)
    {
        var result = [];
        if(search_text)
        {
            angular.forEach(data, function(value,key)
            {
                var ttl = value.title.toLowerCase();
                var tag = value.tag.toLowerCase();
                var url = value.url.toLowerCase();
                
                if(ttl.indexOf(search_text.toLowerCase()) > -1
                        || url.indexOf(search_text.toLowerCase()) !== -1
                        || tag.indexOf(search_text.toLowerCase()) !== -1)
                {
                    result.push(value);
                }
            });
            return result;
        }
        else
        {
            return data;
        }
    }
});
